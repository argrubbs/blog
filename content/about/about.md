---
title: "About Me"
draft: false
---

{{< rawhtml >}}
    <img src="/media/me_resize.jpg" style="float: right;max-height: 250px;" />
{{< /rawhtml >}}

This is me. My name is Adam Grubbs.

I've been a tech enthusiast for almost all my life. I've used a variety of devices, software, platforms, and paradigms. I work professionally as a Linux admin doing basically anything my boss tasks me with, from virtualization to devops automations.

I've been a long time Linux user and recently got back into MacOS. I am an avid fan of the iPad Pro.

While I'm not really competitive, I do consider myself a gamer. Recently I have become enamored with game streaming technologies like Google Stadia and GeForce NOW.