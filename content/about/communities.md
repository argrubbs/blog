---
title: "Communities"
draft: false
---

## Places You Can Find Me

[Twitter](https://twitter.com/argrubbs)
[GitLab](https://gitlab.com/argrubbs/)
[Telegram](https://bigdaddylinux.com/telegram)
[Discourse](https://discourse.bigdaddylinux.com)

## Content I Enjoy

Podcasts:
- [Windows Weekly](https://twit.tv/ww/)
- [Linux Spotlight](https://bigdaddylinux.com/linux-spotlight/)
- [MacBreak Weekly](https://twit.tv/mbw/)
- [Late Night Linux](https://latenightlinux.com)
- [StadiaCast](https://anchor.fm/stadiacast/)
- [DLN Xtend](https://dlnxtend.com/)

YouTube:
- [Eric Adams](https://www.youtube.com/channel/UCcqoUM4UUvc9cFK-jR5Nx9A)
- [Wimpy's World](https://www.youtube.com/channel/UChpYmMp7EFaxuogUX1eAqyw)
- [Digital Foundry](https://www.youtube.com/user/DigitalFoundry)
- [HexDSL](https://www.youtube.com/channel/UCRE3NFNtdjR96-H4QG4U1Fg)
- [Technology Connections](https://www.youtube.com/channel/UCy0tKL1T7wFoYcxCe0xjN6Q)
- [Todd In The Shadows](https://www.youtube.com/channel/UCaTSjmqzOO-P8HmtVW3t7sA)
- [Ahoy](https://www.youtube.com/user/XboxAhoy)

Twitch:
- [HexDSL](https://twitch.tv/hexdsl)
- [Lilypichu](https://twitch.tv/lilypichu)
- [Trihex](https://twitch.tv/trihex)
- [Archtoasty](https://twitch.tv/archtoasty)
- [KylinuxCast](https://twitch.tv/kylinuxcast)

Blogs:
- [Thurrott.com](https://thurrott.com)
- [The Verge](https://theverge.com)
- [ArsTechnica](https://arstechnica.com)
- [FrontPageLinux](https://frontpagelinux.com)
- [OMGUbuntu!](https://omgubuntu.co.uk)


