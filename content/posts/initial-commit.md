+++
title = "Initial Commit"
date = "2020-04-05T15:17:59-04:00"
author = "Adam Grubbs"
authorTwitter = "argrubbs" #do not include @
cover = ""
keywords = ["intro", "new beginnings"]
description = "My old site died. Here's how we continue on."
showFullContent = false
+++

## New Site, New Me

So, to a very few people this site address might look familiar. I had a previous WordPress site that I kept some writings on previously. This site was hosted on DigitalOcean in one of their easy-to-setup application droplets. It was fairly simple to set up and get going. Maintaining it wasn't too hard given my profession, but it could be a hassle keeping everything working. It was nice, it was pretty, it was solving my problem.

Then everything went wrong.

I went to my site URL one day to find that it was not coming up. Confused, I checked my client settings and caches to find nothing wrong. So, off to the server I go! I attempted to SSH into it, but it was not going through, just timing out over and over again. My next destination was the DigitalOcean dashboard and the console itself.

I discovered that my droplet had encountered a kernel panic. Not something I thought would happen given that it was a simple install of WordPress and Ubuntu. Being an experienced Linux user I went into troubleshooting mode. I booted into a recovery environment and ran updates thinking that perhaps something in a previous update had gone wrong. I updated, rebooted, and then it panicked again. I attempted to roll back some updates, but was not successful. I attempted to restore from snapshot, but it, too, was borked. Not a good feeling.

Now, at work I spend an inordinate amount of effort making sure that redundancy, failover, and backups are in place for everything I use. On my personal stuff, I guess not so much. I did not have backups of my site. I didn't have a lot on there, but I certainly have learned that lesson. Won't happen again.

## Hugo To The Rescue

This chance at a new site gave me a chance to try a new approach. I had heard a bit about static site generators like Hugo and Jekyll and wanted to give one a try.

I decided on Hugo because it appeared to be a simple solution, but also because it was well integrated in my hosting choice: GitLab Pages. GitLab is my git service provider of choice because of the quality of the software and the freedom that their platform offers. GitHub is a great platform, too. Arguably it has more integrations with the outside world. GitLab is the tool we use at work and I am more comfortable with it overall, so I went with it.

I set up my custom domain with GitLab to point to my Pages repo. It is an easy process with HTTPS built-in using LetsEncrypt and offering a simple interface to get things going. There are even templates to help you get your static site going.

With GitLab CI/CD every time I push to master on my git repo the pipeline builds and deploys my site to their webserver. This makes managing it really easy and customizable for my needs. No server updates to manage, and the pipeline keeps the Hugo install up to date.

I am familiar with using a git-based workflow for code and configs at work, so moving to one for my site content feels just right. I can even work on my iPad Pro in Textastic and Working Copy. (More on that at a later time)

So, we're back in action now and ready to push out some posts. Hopefully I can get some readers this time.

{{< youtube 927wgzzNMEA >}}